<?php

class Produto extends CrudPDO{
    
    protected $table = 'produto';
    
    protected $fields = ['nome'];
}
