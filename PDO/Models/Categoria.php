<?php


class Categoria extends CrudPDO {

    protected $table = 'categoria';
    
    protected $fields = ['nome'];
}
