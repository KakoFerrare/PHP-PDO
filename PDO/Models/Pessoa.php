<?php

class Pessoa extends CrudPDO{

    protected $table = 'pessoa';
    
    protected $fields = ['nome','email','idade'];
}
