<?php

require_once './DB/PDOConnect.class.php';
require_once './config/database.php';
require_once './DB/CrudPDO.php';
require_once './Models/Aluno.php';
require_once './Models/Turma.php';


$pdo = new PDOConnect;
$conn = $pdo->connect();

$aluno = new Aluno('alunos', $conn);

//print_r($aluno->select('alunos.*, turmas.nome as turma')->join('turmas', 'alunos.id_turma = turmas.id')->get());
//print_r($aluno->select('alunos.*, turmas.nome as turma')->join('turmas', 'alunos.id_turma = turmas.id')->where('alunos.id', '1')->get());
print_r($aluno->select('alunos.nome as aluno, turmas.*')->join('turmas', 'alunos.id_turma = turmas.id')->where('alunos.id', '1')->get());