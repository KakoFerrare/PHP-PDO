<?php

require_once './DB/PDOConnect.class.php';
require_once './config/database.php';
require_once './DB/CrudPDO.php';
require_once './Models/Aluno.php';
require_once './Models/Turma.php';


$pdo = new PDOConnect;
$conn = $pdo->connect();

$turma = new Turma('turmas', $conn);

//print_r($turma->select('turmas.nome, turmas.id')->get());
//print_r($turma->select('turmas.nome, turmas.id')->where('turmas.id', 1)->get());
//print_r($turma->select('turmas.nome, turmas.id')->where('turmas.id', 1)->orWhere('turmas.id', 2)->get());
//print_r($turma->select('alunos.*')->join('alunos', 'alunos.id_turma = turmas.id')->where('turmas.id', '1')->get());
//print_r($turma->select('turmas.*')->orderBy('turmas.nome, turmas.id', 'DESC')->get());
//print_r($turma->select('turmas.*')->orderBy('turmas.nome')->limit(1)->get());
print_r($turma->select('turmas.nome')->distinct()->get());
