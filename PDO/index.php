<?php

require_once './DB/PDOConnect.class.php';
require_once './config/database.php';

$pdo = new PDOConnect;
$conn = $pdo->connect();

//CONSULTA, SEM TRATAMENTO DA QUERY
//$query = $conn->query("SELECT * FROM pessoa");


//---------------------------------IMPORTANTE(FORMA MAIS COMPLEXA)---------------------------------
/*TRATAMENTO OU PREPARAÇÃO DA QUERY PARA IMPEDIR SQL INJECTION
A ORDEM DOS PONTOS DE PERGUNTAS EQUIVALEM A POSIÇÃO DELES TEMOS 2 ? ENTÃO:
ID = ? (PRIMEIRO PARAMETRO)
NOME = ? (SEGUNDO PARAMETRO)

$query = $conn->prepare("SELECT * FROM pessoa where id = ? OR nome = ?");

ORDEM DAS CLAUSULAS DA QUERY:
1 - O ? QUE ELE EQUIVALE, SE É O PRIMEIRO, SEGUNDO ETC...
2 - O VALOR DO PARAMETRO
3 - O TIPO DO PARAMETRO
$query->bindValue(1,'1', PDO::PARAM_INT);
$query->bindValue(2,'Kako',PDO::PARAM_STR);

DEPOIS DE PREPARAR E PASSAR OS PARAMETROS EU EXECUTO
$query->execute();
------------------------------------------------------------------------*/


//---------------------------------IMPORTANTE(FORMA MAIS SIMPLES)---------------------------------
$query = $conn->prepare("SELECT * FROM pessoa where id = :id OR nome = :nome");
$query->bindValue(':id','1', PDO::PARAM_INT);
$query->bindValue(':nome','Kako',PDO::PARAM_STR);
$query->execute();



//var_dump($query->fetchAll(PDO::FETCH_OBJ));

//COM O FETCH_OBJ ELE RETORNA OBJETOS CONFORME OS RESULTADOS
foreach($query->fetchAll(PDO::FETCH_OBJ) as $pessoa){
    echo "Nome: {$pessoa->nome} <br>";
}

//COMO O FETCHALL RETORNA UM ARRAY SERIA FEITO DESSA FORMA
//foreach($query->fetchAll() as $pessoa){
//    echo "Nome: {$pessoa['nome']} <br>";
//}