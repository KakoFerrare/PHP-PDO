<?php

require_once './DB/PDOConnect.class.php';
require_once './config/database.php';
require_once './DB/CrudPDO.php';
require_once './Models/Categoria.php';
require_once './Models/Produto.php';


$pdo = new PDOConnect;
$conn = $pdo->connect();

$produto = new Produto('produto', $conn);
print_r($produto->select('categoria.*')->manyToMany('produto_has_categoria', 'produtos_id', 'categoria', 'categorias_id')->get());

//$categoria = new Categoria('categorias', $conn);
//print_r($categoria->select('produtos.*')->manyToMany('produto_has_categoria', 'categorias_id', 'produto', 'produtos_id')->get());
