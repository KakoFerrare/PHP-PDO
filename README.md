Estudo de PHP com o classe PDO


Query das tabelas que criei:

create table turmas (
id INT NOT null AUTO_INCREMENT,
nome varchar(100) NOT NULL,
primary key (id)
);

create table alunos (
id INT NOT null AUTO_INCREMENT,
id_turma INT NOT NULL,
nome varchar(100) NOT NULL,
email varchar(150) NOT NULL,
primary key (id),
foreign key(id_turma) references turmas(id)
);
